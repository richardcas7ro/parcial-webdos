import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTweetsComponent } from './create-tweets.component';

describe('CreateTweetsComponent', () => {
  let component: CreateTweetsComponent;
  let fixture: ComponentFixture<CreateTweetsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTweetsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTweetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
