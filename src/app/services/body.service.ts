import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BodyService {

   url = environment.url
  constructor(private http: HttpClient) { }

getAllposts(){


 // get AllPost http://localhost:3000/posts/
 return this.http.get(this.url);
}

getPostbyUser(username){

  return this.http.get('http://localhost:3000/posts/'+username);

}


}
