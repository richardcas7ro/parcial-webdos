import { TestBed } from '@angular/core/testing';

import { EnviarPostService } from './enviar-post.service';

describe('EnviarPostService', () => {
  let service: EnviarPostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EnviarPostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
