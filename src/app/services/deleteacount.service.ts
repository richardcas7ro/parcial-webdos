import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeleteacountService {

  constructor(private http:HttpClient) { }



  deleteUser(username){
    return this.http.delete("http://localhost:3000/users/"+username)
  }

}
