import { TestBed } from '@angular/core/testing';

import { DeleteacountService } from './deleteacount.service';

describe('DeleteacountService', () => {
  let service: DeleteacountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeleteacountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
