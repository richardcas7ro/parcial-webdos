import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class EnviarPostService {

  constructor(private http: HttpClient) { }


 
send_post(information){
  return this.http.post('http://localhost:3000/posts/',information);
}
}