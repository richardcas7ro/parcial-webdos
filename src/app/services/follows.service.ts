import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EnviarPostService } from '../services/enviar-post.service';

@Injectable({
  providedIn: 'root'
})
export class FollowsService {

  constructor(private http: HttpClient,private post:EnviarPostService) { }

  sendfollos(user){
    return this.http.get('http://localhost:3000/follow/'+user);
  }

  insertfollow(information2){

    return this.http.post('http://localhost:3000/follow/',information2);

  }
  
}
