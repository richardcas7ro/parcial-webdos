import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { NewUser } from '../models/newUser.model';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<NewUser[]>(`/users/`);
    }

    register(user: NewUser) {
        return this.http.post(`/users/`, user);
    }

    delete(id: number) {
        return this.http.delete(`/users/${id}`);
    }
}