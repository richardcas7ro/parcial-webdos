import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders, HttpParams } from '@angular/common/http'; 
import { NewUser } from '../models/newUser.model';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<NewUser>;
    public currentUser: Observable<NewUser>;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<NewUser>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): NewUser {
        return this.currentUserSubject.value;
    }

    login(username, password) {

      let httpHeaders = new HttpHeaders()
                         .set('Accept', 'application/json');
      let httpParams = new HttpParams()
                        .set('username', username)
                          .set('password', password);
                                        
        return this.http.get<any>('http://localhost:3000/users/login/', {
          headers: httpHeaders,
          params: httpParams, 
          responseType: 'json'
      }) 
          .pipe(map(user => {

           /* 
            console.log("[22]"+user);
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                
                if (user && user.token) { //
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                }

                return user;
*/
            localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;

            }))
            ;
            
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }
}