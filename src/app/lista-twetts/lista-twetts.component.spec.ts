import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaTwettsComponent } from './lista-twetts.component';

describe('ListaTwettsComponent', () => {
  let component: ListaTwettsComponent;
  let fixture: ComponentFixture<ListaTwettsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaTwettsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTwettsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
