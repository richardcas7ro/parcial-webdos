import { Component, OnInit } from '@angular/core';
import {TweetsService} from '../tweets.service';
import { BodyService } from '../services/body.service';
import { FollowsService } from '../services/follows.service';
import{follow} from '../models/follow.model';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-lista-twetts',
  templateUrl: './lista-twetts.component.html',
  styleUrls: ['./lista-twetts.component.css']
})
export class ListaTwettsComponent implements OnInit {

//misTweets;
postslist: [];

follows:follow;


  /**
 * Constructor
 */
  constructor(private Postsservice: BodyService, private follow_user:FollowsService) { 
  this.postslist=[];
  this.follows= new follow;
  this.getposts();
 
  

  }

  ngOnInit(): void {
  }
   getposts(){

   this.Postsservice.getAllposts().subscribe(
     (data)=>{

      console.log('Data',data);
      
      this.postslist=data['data'];

     },
     (error)=>{
       console.error('Fallo en la comunicacion')
     }


   );

   }
   insertfollows(username:string){
     
    this.follows.userfollow=localStorage.getItem('session_user');
      this.follows.username=username;
      let id  = this.follows.username;
    this.follow_user.insertfollow(this.follows).subscribe();
      this.follows= new follow;
    }

    
  

  
}
