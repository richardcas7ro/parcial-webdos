import { Component, OnInit } from '@angular/core';
import {Tweet} from '../models/tweet.model';
import { EnviarPostService } from '../services/enviar-post.service';
import { FollowsService } from '../services/follows.service';
import {User} from '../models/user.model';
import { ActivatedRoute, Router } from '@angular/router';
import{BodyService} from '../services/body.service';
import { DeleteTweetService } from '../services/delete-tweet.service';
import { DeleteacountService } from '../services/deleteacount.service';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
 
 
  minewTweet:Tweet;
  postlist:[];
  biid:[];
 
  Usuario:User; 
  userid;
  idtweet;

  constructor(
    private postsend:EnviarPostService,
    private activeRoute:ActivatedRoute,
    private follow_user:FollowsService,
    private postservice:BodyService,
    private deleteservice:DeleteTweetService,
    private deleteacount:DeleteacountService ,
    private router: Router
    ) {
  
  
  this. minewTweet = new Tweet;
  this.postlist=[];
  this.biid=[];
  this.Usuario= new User;
  this.seefollows();
  this.userid=localStorage.getItem("session_user");
  
 
 

   }

  ngOnInit(): void {
    this.seepostbyid();
  }

  addPost(){
    this.minewTweet.username=this.userid;
   this.postsend.send_post(this.minewTweet).subscribe(
    
   );
   console.log(this.minewTweet);
    this.minewTweet = new Tweet;
  }

  seefollows(){
    
    this.follow_user.sendfollos(this.userid).subscribe(
      (data)=>{

        console.log('Data',data);
        this.postlist=data['data'];
        
  
       },
       (error)=>{
         console.error('Fallo en la comunicacion')
       }
  
  
     );
    
       this.Usuario= new User;
  }


  seepostbyid(){

    this.postservice.getPostbyUser(this.userid).subscribe(
      (data)=>{
 
       console.log('Data',data);
       
       this.biid=data['data'];
 
      },
      (error)=>{
        console.error('Fallo en la comunicacion')
      }
 
 
    );

  }
  deleteTweetById(id:number){
    this.idtweet=id;
    console.log(this.idtweet);
    this.deleteservice.deletetweet(this.idtweet).subscribe();
  }
  navegar2(){
    this.router.navigate(['/perfil'])
  }


  deleteAcount(){
  
    console.log(this.userid);
    this.deleteacount.deleteUser(this.userid).subscribe();
  }
  navegar(){
    this.router.navigate(['/Login'])
  }

  

}
