import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListaTwettsComponent } from './lista-twetts/lista-twetts.component';
import { CreateTweetsComponent } from './create-tweets/create-tweets.component';
import { TweetComponent } from './tweet/tweet.component';
import { LoginComponent } from './login/login.component';
import{RegistroComponent} from './registro/registro.component';
import{PerfilComponent} from './perfil/perfil.component'


const routes: Routes = [

{path:'',redirectTo: '/Login',pathMatch:'full'},
{path:'Login',component:LoginComponent},
{path:'listaTweets',component:ListaTwettsComponent},
{path:'new-tweet',component:TweetComponent},
{path:'registro',component:RegistroComponent},
{path:'perfil/:id',component:TweetComponent},
{path:'perfil',component:PerfilComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
