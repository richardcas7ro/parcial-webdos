import { Component, OnInit } from '@angular/core';
import { RegistroService } from '../services/registro.service';
import{ NewUser} from '../models/newUser.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  userNew:NewUser;

  constructor(private userService:RegistroService,private router: Router) { 
    this.userNew= new NewUser;
  }

  ngOnInit(): void {
  }

  addUser(){
    this.userNew.autentication=true;
    this.userService.createuser(this.userNew).subscribe(
      
     );

     console.log(this.userNew);

    this.userNew = new NewUser;
    this.router.navigate(['/Login'])


  }
}