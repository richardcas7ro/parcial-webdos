import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { LoginService } from '../services/login.service';

import { GlobalComponent } from '../global/global.component';
import { Router } from '@angular/router';
import { Local } from 'protractor/built/driverProviders';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  title = 'Poliwtter';
  Usuario:User; 
  logeado = false;
  Global:GlobalComponent;
  users:Array<User>;

  constructor(private getsend:LoginService,private router: Router) {
    console.log('El Constructor se ha iniciado');
    this.Usuario= new User;
    this.users= new Array<User>();

   }

  ngOnInit(): void {
    console.log('El Constructor se ha iniciado');
  }

 /* log(): void{
    console.log('paso1')
    this.getsend.log(this.Usuario.username,this.Usuario.password).subscribe(
      (data)=>{
        console.log('Data',data);  
        if (data != null){
          this.Global.setLog(true);
          //this.Global.UsuarioGlo = data.username;
          //this.router.navigate(['/new-tweet']);
        }
       },
       (error)=>{
         console.error('Fallo en la comunicacion')
       }
    );

   
  }
  /*getuserid(username:any){
    this.Usuario.username=username;
    console.log(this.Usuario.username);
  }*/

  getusers(){
    console.log("estoes",this.Usuario);
    this.getsend.send_users().subscribe(
      (data)=>{
 
       this.users=data['data'];

       for(let i=0;i < this.users.length;i++){
        if(this.users[i].password == this.Usuario.password && this.users[i].username == this.Usuario.username){
              localStorage.setItem('session_user',this.Usuario.username)
              this.navegar();
              return
        }
      }
      alert("DATOS ERRONEOS")
 
      },
      (error)=>{
        console.error('Fallo en la comunicacion')
      },() =>{
       
      }
 
 
    );


  }
  
navegar(){
  this.router.navigate(['/listaTweets']);
}
navegar2(){
  this.router.navigate(['/registro'])
}
}
