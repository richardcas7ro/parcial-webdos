import { Component, OnInit } from '@angular/core';
import {Tweet} from '../models/tweet.model';
import { EnviarPostService } from '../services/enviar-post.service';
import { FollowsService } from '../services/follows.service';
import {User} from '../models/user.model';
import { ActivatedRoute } from '@angular/router';
import { BodyService } from '../services/body.service';

@Component({
  selector: 'app-tweet',
  templateUrl: './tweet.component.html',
  styleUrls: ['./tweet.component.css']
})
export class TweetComponent implements OnInit {

 

  postlist:[];
  biid:[];
 
  
  userid;

  constructor(private activeRoute:ActivatedRoute,private follow_user:FollowsService, private postservice: BodyService) { 
    
    this.postlist=[];
    
    this.seefollows();
    this.biid=[];
   
   

 
  }
 

  seefollows(){
    
    this.follow_user.sendfollos(this.userid).subscribe(
      (data)=>{

        console.log('Data',data);
        this.postlist=data['data'];
        
  
       },
       (error)=>{
         console.error('Fallo en la comunicacion')
       }
  
  
     );
    
      
  }

  seepostbyid(){

    this.postservice.getPostbyUser(this.userid).subscribe(
      (data)=>{
 
       console.log('Data',data);
       
       this.biid=data['data'];
 
      },
      (error)=>{
        console.error('Fallo en la comunicacion')
      }
 
 
    );

  }
  

  
  ngOnInit(): void {

    this.activeRoute.params.subscribe(params =>{
       this.userid = params['id']
    })
    this.seepostbyid();
  }

}
