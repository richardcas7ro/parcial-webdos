import { Component, OnInit } from '@angular/core';
import {User} from '../models/user.model';

@Component({
  selector: 'app-global',
  templateUrl: './global.component.html',
  styleUrls: ['./global.component.css']
})

export class GlobalComponent  {

  UsuarioGlo:User; 
  Login = false;

  constructor() { }

   setUser(user){
    this.UsuarioGlo.username = user;
  }

   setLog(log){
    this.Login =  log;
  }

  ngOnInit(): void {
  }

}
