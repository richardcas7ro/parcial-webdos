import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Poliwtter';
  name = 'Ricardo Castro';
  num1:number;
  num2:number;
  resultSuma:number;

  /**
   * CONSTRUCTOR
   */
  constructor(){
    console.log('El Constructor se ha iniciado');
    
  }




/**
 * Funcion que suma dos numeros
 */


  sumar(){
    // el this es obligatorio para señalar los atributos que estan en la clase
    console.log('Sumar');
    return  this.resultSuma=this.num1+this.num2;

  }




}
